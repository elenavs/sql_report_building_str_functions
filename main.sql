-- Есть две таблицы - LOG и IP, нужно:
-- 1)структурировать данные и построить таблицу посещений --
-- 2)построить отчет в каких регионах какой браузер самый популярный --


-- 1) Парсим таблицы, используя строковые функции --
select * from de.ip;

select dump (val) from de.ip;

select 
substr (val, 1, instr (val, ' ', 1, 1) - 1) t1_ip,
substr (val, instr (val, ' ', 1, 1) + 1) t1_region
from de.ip;
------------------------------------------------------
select * from de.log;

select dump (val) from de.log;

select 
replace (val, chr (9), '!') t1
from de.log;

SELECT
TO_DATE (substr (val, instr (val, chr (9), 1, 3) + 1, 
    instr (val, chr (9), 1, 4) - 
    instr (val, chr (9), 1, 3) - 1), 'YYYYMMDDHH24MISS') t1_dt
FROM de.log;

SELECT 
substr (val, instr (val, chr (9), 1, 4) + 1, 
    instr (val, chr (9), 1, 5) - 
    instr (val, chr (9), 1, 4) - 1) t2_link
FROM de.log;

SELECT 
substr (val, instr (val, chr (9), 1, 7) + 1) t2_user_agent
FROM de.log;

SELECT 
substr (val, 1, instr (val, chr (9), 1, 1) - 1) t1_ip
FROM de.log;

SELECT
TO_DATE (substr (val, instr (val, chr (9), 1, 3) + 1, 
    instr (val, chr (9), 1, 4) - instr (val, chr (9), 1, 3) - 1), 'YYYYMMDDHH24MISS') t2_dt,
substr (val, instr (val, chr (9), 1, 4) + 1, 
    instr (val, chr (9), 1, 5) - instr (val, chr (9), 1, 4) - 1) t2_link,
substr (val, instr (val, chr (9), 1, 7) + 1) t2_user_agent,
substr (val, 1, instr (val, chr (9), 1, 1) - 1) t2_ip
FROM de.log;

-- Создаем свою таблицу посещений с очищенными и структурированными данными --

CREATE TABLE demipt2.suha_log AS
SELECT 
CAST (DT AS DATE) DT,
CAST (LINK AS VARCHAR2 (50)) LINK,
CAST (USER_AGENT AS VARCHAR2 (200)) USER_AGENT,
CAST (REGION AS VARCHAR2 (30)) REGION
FROM (
    SELECT
        substr (val, 1, instr (val, ' ', 1, 1) - 1) ip,
        trim (substr (val, instr (val, ' ', 1, 1) +1)) region
    FROM de.ip
    ) t1
LEFT JOIN
    (
    SELECT
        TO_DATE (substr (val, instr (val, chr (9), 1, 3) + 1, 
            instr (val, chr (9), 1, 4) - instr (val, chr (9), 1, 3) - 1), 'YYYYMMDDHH24MISS') dt,
        substr (val, instr (val, chr (9), 1, 4) + 1, 
            instr (val, chr (9), 1, 5) - instr (val, chr (9), 1, 4) - 1) link,
        substr (val, instr (val, chr (9), 1, 7) + 1) user_agent,
        substr (val, 1, instr (val, chr (9), 1, 1) - 1) ip2
    FROM de.log
    ) t2
ON t1.ip = t2.ip2;

SELECT * FROM demipt2.suha_log;

--------------------------------------------------------------------------------------------------------

-- 2) Построение отчета Популярность браузеров по регионам --

-- готовим данные --

SELECT
    region,
    substr (user_agent, 1, instr (user_agent, '/') -1) browser,
    COUNT (1) quantity
FROM demipt2.suha_log
GROUP BY region, 
        substr (user_agent, 1, instr (user_agent, '/') -1)
ORDER BY region,
        quantity DESC;


SELECT 
region,
MAX (quantity) max_q
FROM
    (
    SELECT
    region,
    substr (user_agent, 1, instr (user_agent, '/') -1) browser,
    COUNT (1) quantity
    FROM demipt2.suha_log
    GROUP BY region, 
            substr (user_agent, 1, instr (user_agent, '/') -1)
    ORDER BY region,
            quantity DESC
        )
GROUP BY region;

--строим отчет--

CREATE TABLE demipt2.suha_log_report AS
SELECT 
CAST (region AS VARCHAR2 (30)) region,
CAST (browser AS VARCHAR2 (10)) browser
FROM
    (SELECT
    region,
    substr (user_agent, 1, instr (user_agent, '/') -1) browser,
    COUNT (1) quantity
    FROM demipt2.suha_log
    GROUP BY region, 
        substr (user_agent, 1, instr (user_agent, '/') -1)
    ORDER BY region,
        quantity DESC) table1
INNER JOIN (SELECT 
            region region1,
            MAX (quantity) max_q
            FROM
                (
                SELECT
                region,
                substr (user_agent, 1, instr (user_agent, '/') -1) browser,
                COUNT (1) quantity
                FROM demipt2.suha_log
                GROUP BY region, 
                        substr (user_agent, 1, instr (user_agent, '/') -1)
                ORDER BY region,
                        quantity DESC
                    )
            GROUP BY region) table2
ON table1.region = table2.region1
WHERE quantity = max_q
ORDER BY region;

SELECT * FROM demipt2.suha_log_report;
